# Carpoolingv2
Este proyecto fue realizado bajo los requerimientos del reto 2 del BootCamp PowerUp Java x AWS y su funcionalidad es crear un registro en BD MySQL de una ruta + usuario(conductor) + viajes + barrios por medio de un servicio REST y teniendo en cuenta las validaciones requeridas.

## End Point
Post: localhost:8080/ruta/guardarRutaCompleta

## JSON de la peticion:
```json

{
    "descripcion":"Norte-Sur",
    "cupos":4,
    "usuario":{
        "nombres":"Ana",
        "apellidos":"Soto",
        "email":"asoto@email.com"
    },
    "barriosList":[
    
        {
            "nombre":"Santa Ana",
            "descripcion":"Norte"
        },
        {
            "nombre":"San Jose",
            "descripcion":"Sur"
        }
    ],
    "viajesList":[
        {
            "horario":"2017-01-18T17:09:42.411"
        },
        {
            "horario":"2017-01-13T17:09:42.411"
        }
    ]
}

```
## NOTA:
Antes de compilar el proyecto recuerde configurar el archivo application.properties según sus configuraciones locales