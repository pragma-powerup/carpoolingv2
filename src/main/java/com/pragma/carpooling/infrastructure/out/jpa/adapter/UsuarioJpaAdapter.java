package com.pragma.carpooling.infrastructure.out.jpa.adapter;

import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.infrastructure.exception.UserNotFoundException;
import com.pragma.carpooling.infrastructure.out.jpa.mapper.UsuarioEntityMapper;
import com.pragma.carpooling.infrastructure.out.jpa.repository.IUsuarioRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UsuarioJpaAdapter implements IUsuarioPersistencePort {

    private final IUsuarioRepository usuarioRepository;
    private final UsuarioEntityMapper usuarioEntityMapper;

    @Override
    public Usuario guardarUsuario(Usuario usuario) {
        if(usuarioRepository.findByEmail(usuario.getEmail()).isPresent()){
            //No hay necesidad de persistir nuevamente el usuario
            return obtenerUsuario(usuario.getEmail());
        }
        return usuarioEntityMapper.toUsuario(usuarioRepository.save(usuarioEntityMapper.toEntity(usuario)));
    }


    @Override
    public Usuario obtenerUsuario(String emailUsuario) {
        return usuarioEntityMapper.toUsuario(usuarioRepository.findByEmail(emailUsuario)
                .orElseThrow(UserNotFoundException::new));
    }

}
