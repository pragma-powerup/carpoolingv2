package com.pragma.carpooling.infrastructure.out.jpa.adapter;

import com.pragma.carpooling.domain.model.Barrio;
import com.pragma.carpooling.domain.spi.IBarrioPersistencePort;
import com.pragma.carpooling.infrastructure.exception.BarrioNotFoundException;
import com.pragma.carpooling.infrastructure.out.jpa.mapper.BarrioEntityMapper;
import com.pragma.carpooling.infrastructure.out.jpa.repository.IBarrioRepository;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class BarrioJpaAdapter implements IBarrioPersistencePort {

    private final IBarrioRepository barrioRepository;
    private final BarrioEntityMapper barrioEntityMapper;

    @Override
    public Barrio guardarBarrio(Barrio barrio) {

        if(barrioRepository.findByNombre(barrio.getNombre()).isPresent() && barrioRepository.findByDescripcion(barrio.getDescripcion()).isPresent()){
            return obtenerBarrio(barrio);
        }
        return barrioEntityMapper.toBarrio(barrioRepository.save(barrioEntityMapper.toEntity(barrio)));
    }

    public Barrio obtenerBarrio(Barrio barrio){
        return barrioEntityMapper.toBarrio(barrioRepository.findByNombre(barrio.getNombre()).orElseThrow(BarrioNotFoundException::new));
    }

}
